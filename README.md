# GOS5 --> GOSH
## Project has moved/renamed

The project is now called **GOSH**: **G**eneralised **O**scillator **S**trengths in **H**DF5.

Please find it here: https://gitlab.com/gguzzina/gosh
